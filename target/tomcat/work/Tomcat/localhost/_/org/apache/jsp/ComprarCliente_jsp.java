/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.37
 * Generated at: 2023-10-02 19:22:27 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import br.com.gymcontrol.Model.Produtos;

public final class ComprarCliente_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("\t<meta charset=\"UTF-8\">\r\n");
      out.write("\t<title>Detalhes produto</title>\r\n");
      out.write("\t<link rel=\"stylesheet\" href=\"css/ComprarCliente.css\">\r\n");
      out.write("\t<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css\">\r\n");
      out.write("\t<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>\r\n");
      out.write("\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js\"></script>\r\n");
      out.write("\t<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js\"></script>\r\n");
      out.write("</head>\r\n");
      out.write("<style>\r\n");
      out.write("    .product-description {\r\n");
      out.write("        max-width: 300px; /* Defina o valor máximo de largura desejado */\r\n");
      out.write("        overflow: hidden;\r\n");
      out.write("        white-space: nowrap;\r\n");
      out.write("        text-overflow: ellipsis;\r\n");
      out.write("    }\r\n");
      out.write("\r\n");
      out.write("    .product-image {\r\n");
      out.write("        width: 300px; /* Defina o tamanho horizontal desejado */\r\n");
      out.write("        height: 300px; /* Mantenha o mesmo valor que o tamanho horizontal para manter a imagem quadrada */\r\n");
      out.write("        overflow: hidden;\r\n");
      out.write("    }\r\n");
      out.write("\r\n");
      out.write("    .product-image img {\r\n");
      out.write("        width: 100%; /* Ajusta a largura da imagem para preencher a div de imagem */\r\n");
      out.write("        height: auto; /* Altura automática para manter a proporção original da imagem */\r\n");
      out.write("    }\r\n");
      out.write("\r\n");
      out.write("    .product-description, .product-name {\r\n");
      out.write("        max-width: 300px; /* Defina o valor máximo de largura desejado */\r\n");
      out.write("        overflow: hidden;\r\n");
      out.write("        white-space: nowrap;\r\n");
      out.write("        text-overflow: ellipsis;\r\n");
      out.write("    }\r\n");
      out.write("\r\n");
      out.write("</style>\r\n");
      out.write("<body>\r\n");
      out.write("<div id=\"container\">\r\n");
      out.write("\t<div class=\"row-container-logo-menu\">\r\n");
      out.write("\t\t<div class=\"col-12 col-sm-12 col-md-12 col-xl-12\">\r\n");
      out.write("\t\t\t<!-- Conteúdo da coluna -->\r\n");
      out.write("\t\t\t<div class=\"slideshow-container\">\r\n");
      out.write("\t\t\t\t<img class=\"slideshow-image active\" src=\"img/Fundo2.jpg\">\r\n");
      out.write("\t\t\t\t<img class=\"slideshow-image\" src=\"img/Fundo3.jpg\">\r\n");
      out.write("\t\t\t\t<img class=\"slideshow-image\" src=\"img/Fundo5.jpg\">\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t\t<nav>\r\n");
      out.write("\t\t\t\t<ul class=\"menu\">\r\n");
      out.write("\t\t\t\t\t<li class=\"menu-logo\">\r\n");
      out.write("\t\t\t\t\t\t<img src=\"img/Logo de cafe.png\">\r\n");
      out.write("\t\t\t\t\t\t<h1>BEM-VINDO AO BREWMASTERS CAFÉ.BACKOFFICE</h1>\r\n");
      out.write("\t\t\t\t\t</li>\r\n");
      out.write("\t\t\t\t</ul>\r\n");
      out.write("\t\t\t</nav>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t<div class=\"row-navbar\">\r\n");
      out.write("\t\t<div class=\"col-12 col-sm-12 col-md-12 col-xl-12\">\r\n");
      out.write("\t\t\t<div class=\"navbar\">\r\n");
      out.write("\t\t\t\t<a href=\"##\">\r\n");
      out.write("\t\t\t\t\t<svg xmlns=\"http://www.w3.org/2000/svg\" height=\"1em\" viewBox=\"0 0 448 512\"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><style>svg{fill:#043486}</style><path d=\"M224 256A128 128 0 1 0 224 0a128 128 0 1 0 0 256zm-45.7 48C79.8 304 0 383.8 0 482.3C0 498.7 13.3 512 29.7 512H418.3c16.4 0 29.7-13.3 29.7-29.7C448 383.8 368.2 304 269.7 304H178.3z\"/></svg>\r\n");
      out.write("\t\t\t\t\tLogin\r\n");
      out.write("\t\t\t\t</a>\r\n");
      out.write("\t\t\t\t<a href=\"#\">\r\n");
      out.write("\t\t\t\t\t<svg xmlns=\"http://www.w3.org/2000/svg\" height=\"1em\" viewBox=\"0 0 640 512\"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><style>svg{fill:#ffffff}</style><path d=\"M96 128a128 128 0 1 1 256 0A128 128 0 1 1 96 128zM0 482.3C0 383.8 79.8 304 178.3 304h91.4C368.2 304 448 383.8 448 482.3c0 16.4-13.3 29.7-29.7 29.7H29.7C13.3 512 0 498.7 0 482.3zM504 312V248H440c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V136c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H552v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z\"/></svg>\r\n");
      out.write("\t\t\t\t\tCadastrar\r\n");
      out.write("\t\t\t\t</a>\r\n");
      out.write("\t\t\t\t<a href=\"http://localhost:8080/\">\r\n");
      out.write("\t\t\t\t\t<svg xmlns=\"http://www.w3.org/2000/svg\" height=\"1em\" viewBox=\"0 0 512 512\"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d=\"M48.5 224H40c-13.3 0-24-10.7-24-24V72c0-9.7 5.8-18.5 14.8-22.2s19.3-1.7 26.2 5.2L98.6 96.6c87.6-86.5 228.7-86.2 315.8 1c87.5 87.5 87.5 229.3 0 316.8s-229.3 87.5-316.8 0c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0c62.5 62.5 163.8 62.5 226.3 0s62.5-163.8 0-226.3c-62.2-62.2-162.7-62.5-225.3-1L185 183c6.9 6.9 8.9 17.2 5.2 26.2s-12.5 14.8-22.2 14.8H48.5z\"/></svg>\r\n");
      out.write("\t\t\t\t\tVoltar\r\n");
      out.write("\t\t\t\t</a>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t<div id=\"container-h1\">\r\n");
      out.write("\t\t<div class=\"row-h1-menu\">\r\n");
      out.write("\t\t\t<div class=\"col-12 col-sm-12 col-md-12 col-xl-12\">\r\n");
      out.write("\t\t\t\t<h1>Detalhes do Produto</h1>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t<br><br>\r\n");
      out.write("\t<div class=\"produto-container\">\r\n");
      out.write("\t\t");

			Produtos produto = (Produtos) request.getAttribute("produto");
			if (produto != null) {
				String nomeProduto = produto.getNomeProduto();
				String descricaoDetalhada = produto.getDescricaoDetalhada();
				double avaliacao = produto.getAvaliacao();
				double precoProduto = produto.getPrecoProduto();
				int qtdEstoque = produto.getQtdEstoque();
		
      out.write("\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t<!-- Exibe todas as imagens do produto -->\r\n");
      out.write("\t\t<div id=\"imageCarousel\" class=\"carousel slide\" data-ride=\"carousel\">\r\n");
      out.write("\t\t\t<div class=\"carousel-inner\">\r\n");
      out.write("\t\t\t\t");

					boolean firstImage = true;
					for (String imagePath : produto.getImagens()) {
				
      out.write("\r\n");
      out.write("\t\t\t\t<div class=\"carousel-item ");
      out.print( firstImage ? "active" : "" );
      out.write("\">\r\n");
      out.write("\t\t\t\t\t<img src=\"");
      out.print( imagePath );
      out.write("\" alt=\"Imagem do Produto\">\r\n");
      out.write("\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t");

						firstImage = false;
					}
				
      out.write("\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t\t\r\n");
      out.write("\t\t\t<!-- Controles do Carrossel -->\r\n");
      out.write("\t\t\t<a class=\"carousel-control-prev\" href=\"#imageCarousel\" role=\"button\" data-slide=\"prev\">\r\n");
      out.write("\t\t\t\t<span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>\r\n");
      out.write("\t\t\t\t<span class=\"sr-only\">Anterior</span>\r\n");
      out.write("\t\t\t</a>\r\n");
      out.write("\t\t\t<a class=\"carousel-control-next\" href=\"#imageCarousel\" role=\"button\" data-slide=\"next\">\r\n");
      out.write("\t\t\t\t<span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>\r\n");
      out.write("\t\t\t\t<span class=\"sr-only\">Próximo</span>\r\n");
      out.write("\t\t\t</a>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t<div class=\"product-details\">\r\n");
      out.write("\t\t\t<h2>");
      out.print( nomeProduto );
      out.write("\r\n");
      out.write("\t\t\t</h2>\r\n");
      out.write("\t\t\t<p>");
      out.print( descricaoDetalhada );
      out.write("\r\n");
      out.write("\t\t\t</p>\r\n");
      out.write("\t\t\t<p class=\"avaliacao\">Avaliação:\r\n");
      out.write("\t\t\t\t");

					// Converte a avaliação em estrelas
					int avaliacaoEmEstrelas = (int) Math.floor(avaliacao); // Parte inteira da avaliação
					double parteDecimal = avaliacao - avaliacaoEmEstrelas; // Parte decimal (0.0 a 0.9)
					
					for (int i = 1; i <= 5; i++) {
						if (i <= avaliacaoEmEstrelas) {
				
      out.write("\r\n");
      out.write("\t\t\t\t<img class=\"estrela\" src=\"img/estrela_cheia.png\" alt=\"Estrela Cheia\">\r\n");
      out.write("\t\t\t\t");

				} else if (i == avaliacaoEmEstrelas + 1 && parteDecimal >= 0.5) {
				
      out.write("\r\n");
      out.write("\t\t\t\t<img class=\"estrela\" src=\"img/estrela_meia.png\" alt=\"Estrela Meia\">\r\n");
      out.write("\t\t\t\t");

				} else {
				
      out.write("\r\n");
      out.write("\t\t\t\t<img class=\"estrela\" src=\"img/estrela_vazia.png\" alt=\"Estrela Vazia\">\r\n");
      out.write("\t\t\t\t");

						}
					}
				
      out.write("\r\n");
      out.write("\t\t\t</p>\r\n");
      out.write("\t\t\t<p>Preço R$: ");
      out.print( precoProduto );
      out.write("\r\n");
      out.write("\t\t\t</p>\r\n");
      out.write("\t\t\t<p>Quantidade em Estoque: ");
      out.print( qtdEstoque );
      out.write("\r\n");
      out.write("\t\t\t</p>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t<a class=\"buy-button\" disabled>Comprar</a>\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t");

		} else {
		
      out.write("\r\n");
      out.write("\t\t<p>Produto não encontrado.</p>\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t");

			}
		
      out.write("\r\n");
      out.write("\t</div>\r\n");
      out.write("\t\r\n");
      out.write("\t<footer>\r\n");
      out.write("\t\t© 2023 BREWMASTERS CAFÉ. Todos os direitos reservados.\r\n");
      out.write("\t</footer>\r\n");
      out.write("</div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
